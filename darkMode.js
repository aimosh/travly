let isDarkModeActive = false;
function toggleDarkMode() {
    if (isDarkModeActive) {
        document.documentElement.style.setProperty("--bg-color", "white");
        document.documentElement.style.setProperty("--primary-text-color", "black");
        isDarkModeActive = false;
    } else {
        document.documentElement.style.setProperty("--bg-color", "black");
        document.documentElement.style.setProperty("--primary-text-color", "white");
        isDarkModeActive = true;
    }
    localStorage.setItem("darkMode", isDarkModeActive);
    
    var element = document.body;
    element.classList.toggle("dark-mode");

    var x = document.getElementById("button");
    if (x.innerHTML === "🌙") {
      x.innerHTML = "☀️";
  } else {
      x.innerHTML = "🌙";
  }
}  
const savedDarkMode = localStorage.getItem("darkMode");

if (savedDarkMode === "true") {
 
    toggleDarkMode();
}