import mongoose from "mongoose";

const ReviewSchema = new mongoose.Schema(
  {
    image: {
      type: String,
      required: true
    },
    comment: {
        type: String,
        required: true
    }
  },
  { timestamp: true }
);

export const Review = mongoose.model("Review", ReviewSchema);
