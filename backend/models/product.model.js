import mongoose from "mongoose";

const ProductSchema = new mongoose.Schema(
  {
    id: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true,
      max: 32,
    },
    price: {
      type: Number,
      required: true,
    }
  },
  { timestamp: true }
);

export const Product = mongoose.model("Product", ProductSchema);
