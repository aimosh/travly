import mongoose from 'mongoose';

const CitySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      max: 32,
    },
    image: {
      type: String,
      required: true
    },
    region: {
      type: String,
      required: true
    },
    budget: {
      type: Number,
      required: true,
    },
    foodCost: {
      type: Number,
      required: true,
    },
    accommodationCost: { // corrected spelling
      type: Number,
      required: true,
    },
    transportationCost: {
      type: Number,
      required: true,
    }
  },
  { timestamps: true } // corrected property name
);

export const City = mongoose.model('City', CitySchema);
