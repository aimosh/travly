import mongoose from "mongoose";


export const connectToDatabase = async () => {
  try {
    await mongoose.connect('mongodb://127.0.0.1:27017/travly', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  } catch (error) {
    console.error("Error connecting to the database:", error);
  }
};

