import cors from 'cors';
import express from 'express';
import { connectToDatabase } from './mongodb.js';
import { City } from './models/city.model.js';
import { News } from './models/news.model.js';
import { Product } from './models/product.model.js';
import { Review } from './models/review.model.js';

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

app.get('/api/product', async (req, res) => {
    try {
        const products = await Product.find({});
        if (!products || products.length === 0) {
            return res.status(404).json({ error: 'No products found' });
        }
        res.json({ data: products });
    } catch (error) {
        console.error('Error fetching products:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/api/city', async (req, res) => {
    try {
        const cities = await City.find({});
        if (!cities || cities.length === 0) {
            return res.status(404).json({ error: 'No cities found' });
        }
        res.json({ data: cities });
    } catch (error) {
        console.error('Error fetching cities:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/api/news', async (req, res) => {
    try {
        const news = await News.find({});
        if (!news || news.length === 0) {
            return res.status(404).json({ error: 'No news found' });
        }
        res.json({ data: news });
    } catch (error) {
        console.error('Error fetching news:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.post('/api/review', async (req, res) => {
    try {
        const newReview = new Review(req.body);
        await newReview.save();
        res.status(201).json({ message: 'Review created successfully' });
    } catch (error) {
        console.error('Error creating review:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/api/review', async (req, res) => {
    try {
        const reviews = await Review.find({});
        if (!reviews || reviews.length === 0) {
            return res.status(404).json({ error: 'No reviews found' });
        }
        res.json({ data: reviews });
    } catch (error) {
        console.error('Error fetching reviews:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

const server = async () => {
    try {
        await connectToDatabase();
        console.log('Connected to the database');

        app.listen(port, () => {
            console.log(`Server is running on port ${port}`);
        });
    } catch (error) {
        console.error(error);
    }
};

server();
