import { Product } from "../models/product.model.js";

export const listProduct = async (req, res) => {
  try {
    const data = await Product.find({});
    if(data.length == 0) return res.status(404).json({ error: 'Not found' })
    res.json({data})
  } catch(err) {
    res.status(400).json({ error: err.message })
  }
}