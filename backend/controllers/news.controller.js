import { News } from "../models/news.model.js";

export const listNews = async (req, res) => {
  try {
    const data = await News.find({});
    if(data.length == 0) return res.status(404).json({ error: 'Not found' })
    res.json(data);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};