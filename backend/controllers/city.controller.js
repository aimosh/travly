import { City } from '../models/city.model.js';

export const listCity = async (req, res) => {
  try {
    const data = await City.find({});
    if (data.length === 0) return res.status(404).json({ error: 'Not found' });
    res.json({ data });
  } catch (err) {
    console.error('Error in listCity:', err);
    res.status(400).json({ error: err.message });
  }
};
