import { Review } from "../models/review.model.js";

export const createReview = async (req, res) => {
    try {
      const { comment } = req.body;
      if(!comment) return res.status(401).json({error: 'Request is not complete'});
      const data = await Review.create({comment});
      if(!data) return res.status(400).json({error: 'Database creating error'});
      res.json(data);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  };
  
export const listReview = async (req, res) => {
  try {
    const data = await Review.find({});
    if(data.length == 0) return res.status(404).json({ error: 'Not found' });
    res.json({data});
  } catch(err) {
    res.status(400).json({ error: err.message })
  }
};

export const deleteReview = async (req, res) => {
  try {
    const { id } = req.body;
    const data = await Review.deleteOne({_id:id});
    res.json({data});
  } catch(err) {
    res.status(400).json({ error: err.message })
  }
}

export const deleteReviewAll = async (req, res) => {
  try {
    const data = await Review.deleteMany({});
    res.json({data});
  } catch(err) {
    res.status(400).json({ error: err.message })
  }
}